/************************************************************************************//**
* \file       parser.h
* \brief      This module implements parser for DAQ commands.
* \ingroup    DAQparser
****************************************************************************************/
/************************************************************************************//**
* \defgroup   DAQparser Protocol parser
* \brief      This module implements parser for DAQ commands.
* \ingroup    DAQ
****************************************************************************************/
#ifndef PARSER_H_
#define PARSER_H_


/****************************************************************************************
* Include files
****************************************************************************************/
#include "core.h"                            /* Core functionality for acquisition     */
#include "comInterface.h"                    /* USB interface driver                   */


/****************************************************************************************
* Macro definitions
****************************************************************************************/
/* Supported commands */
/** \brief Command starts ADC timer and enables it’s interrupts.\n
 *         Command string: “S\\n\\r”
 */
#define CMD_START_ACQ               'S'

/** \brief Command stops ADC timer and disables it’s interrupts.\n
 *         Command string: “T\\n\\r”
 */
#define CMD_STOP_ACQ                'T'

/** \brief Command sets how many values will DAQ average before sending them to user.\n
 *         Command string: “A {1}\\n\\r”\n
 *         Parameter 1: Number of values to average\n
 *          ->Minimum value: 0\n
 *          ->Maximum value: 1000\n
 *          ->Default value: 0 (no averaging)
 */
#define CMD_SET_AVERAGE_COUNT       'A'

/** \brief Command sets the time between samples in microseconds.\n
 *         Command string: “R {1}\\n\\r”\n
 *         Parameter 1: Number of microseconds\n
 *          ->Minimum value: 1\n
 *          ->Maximum value: 1000000\n
 *          ->Default value: 1000000
 */
#define CMD_SET_SAMPLE_PERIOD       'R'

/** \brief Command sets how many ADC measurements DAQ will take.\n
 *         Command string: “F {1}\\n\\r”\n
 *         Parameter 1: Number of measurements\n
 *          ->Minimum value: 0\n
 *          ->Maximum value: 1000\n
 *          ->Default value: 0 (continuous mode)
 */
#define CMD_SET_MEASURMENT_COUNT    'F'

/** \brief Command will set the ADC channel sequence. If parameter is 0, no channel is
 *         selected (this is how you can disable channel)\n
 *         Command string: “E {1}, {2}, {3}, {4}\\n\\r”\n
 *         Parameter 1-4: Channel selection\n
 *          ->Minimum value: 0\n
 *          ->Maximum value: 4\n
 *          ->Default value: 1, 2, 3, 4
 */
#define CMD_SET_SEQUENCER           'E'

/** \brief Command sets ADC sending mode.\n
 *         If ASCII is selected, DAQ will send string (“CH%u: %+ 6.0fmV,”, channel, 
 *         measurement)\n
 *         If BIN is selected, DAQ will send measurements in bytes. DAQ first sends 
 *         2 sync bytes (which have MSB 1, other 7 bits are block size), then it 
 *         sends values in a block. Every ADC measurement is written in 2 bytes. 
 *         Bits 15:12 are channel (0,2,4,6), bits 0:11 are value
 *         Command string: “M {1}\\n\\r”\n
 *         Parameter 1: Sending mode\n
 *          ->ASCII mode:  0\n
 *          ->Binary mode: 1\n
 *          ->Default value: 0 (ASCII mode)
 */
#define CMD_SET_MODE                'M'

/** \brief Command sets the gain for specified ADC channel.\n
 *         Command string: “G {1}, {2}\\n\\r”\n
 *         Parameter 1: Channel selection\n
 *          ->Minimum value: 1\n
 *          ->Maximum value: 4\n
 *         Parameter 2: Channel gain selection\n
 *          ->Gain 0.5: 0\n
 *          ->Gain 1.0: 1\n
 *          ->Gain 2.0: 2\n
 *          ->Default value: 1 (gain 1)
 */
#define CMD_SET_ADC_GAIN            'G'

/** \brief Command sets ADC resolution.\n
 *         Command string: “L {1}\\n\\r”\n
 *         Parameter 1: ADC resolution selection\n
 *          ->12 bit: 0\n
 *          ->10 bit: 1\n
 *          ->Default value: 0 (12 bit resolution)
 */
#define CMD_SET_ADC_LOW_RESOLUTION  'L'

/** \brief Command sets the block size for BIN mode.\n
 *         Once number of measurements is equals to block size, DAQ will send 2 sync 
 *         bytes and block of measurements.\n
 *         Command string: “B {1}\\n\\r”\n
 *         Parameter 1: block size\n
 *          ->Minimum value: 1 (will send measurements after each conversion)\n
 *          ->Maximum value: 1024\n
 *          ->Default value: 16
 */
#define CMD_SET_BLOCK_SIZE          'B'

/** \brief Command sets how many times will DAC output LUT (look up table).\n
 *         Will be the same for both channels since this is how DAC PDC works.\n
 *         Command string: “C {1}\\n\\r”\n
 *         Parameter 1: number of repeats\n
 *          ->Minimum value: 0 (continuous mode)\n
 *          ->Maximum value: 0xFFFF\n
 *          ->Default value: 0
 */
#define CMD_SET_REPEAT_NUM          'C'

/** \brief Command sets period of DAC outputting a new LUT value in microseconds.\n
 *         If we have LUT length of 1000 with (1) sinus values and period of 10us, 
 *         frequency of a sinus will be 1/(10us*1000LUT values) = 100Hz.\n
 *         Command string: “H {1}\\n\\r”\n
 *         Parameter 1: Number of microseconds between LUT samples on DAC\n
 *          ->Minimum value: 1\n
 *          ->Maximum value: 65535\n
 *          ->Default value: 2000
 */
#define CMD_SET_DAC_FREQ            'H'

/** \brief Command sends a new value to specified location in LUT.\n
 *         Return: when DAQ successfully receives a new LUT value, it responds with “$”, 
 *         which indicates it can receive a new value.\n
 *         Process: first send LUT length. When received a string starting with “$”, 
 *         send a new LUT value.\n
 *         Location: if you are using fullmode (DAQ outputs to both channels), you have 
 *         to use either odd or even locations (channel 1 would have values on 0,2,4,6… 
 *         DAQ will output channel 1 first).\n
 *         Values: DAC uses tag mode, which means bits 15:12 are channel selection (12th 
 *         bit 1 = channel2, 0 = channel1) and bits 11:0 are DAC value.\n
 *         Command string: “J {1}, {2}\\n\\r”\n
 *         Parameter 1: Location in LUT\n
 *          ->Minimum value: 0\n
 *          ->Maximum value: 2047\n
 *         Parameter 2: LUT value\n
 *          ->Minimum value: 0\n
 *          ->Maximum value: 8191 (channel 2, max DAC value of 4095)\n
 */
#define CMD_SET_DAC_LUT             'J'

/** \brief Command sets the length of LUT. It is used for PDC packet length.\n
 *         This command will return “$LUT length of was set to %u\\n\\r”. $ at the start 
 *         of the string means you can send new LUT value to the DAQ.\n
 *         Command string: “N {1}\\n\\r”\n
 *         Parameter 1: LUT length\n
 *          ->Minimum value: 1\n
 *          ->Maximum value: 2048\n
 *          ->Default value: 1024
 */
#define CMD_DAC_LUT_LENGTH          'N'

/** \brief Command starts DAC timer and enables it’s interrupts.\n
 *         Command string: “O\\n\\r”
 */
#define CMD_DAC_START               'O'

/** \brief Command stops DAC timer and disables it’s interrupts.\n
 *         Command string: “K\\n\\r”
 */
#define CMD_DAC_STOP                'K'

/** \brief Command selects DAC output mode, either fullmode or halfmode.\n
 *         fullmode: DAC PDC will at timer interrupt take 2 values from LUT and put them 
 *         into DAC value register for conversion. This is useful when you want both DAC 
 *         channels to work with DMA. One channel can only have 1024 values in LUT.\n
 *         halfmode: DAC PDC will at timer interrupt take 1 value from LUT and put it 
 *         into DAC value register for conversion. Useful when using only 1 channel. 
 *         This one channel can have 2048 values (more percise) in LUT.\n
 *         Command string: “P {1}\\n\\r”\n
 *         Parameter 1: Transfer mode\n
 *          ->Half mode: 0\n
 *          ->Full mode: 1\n
 *          ->Default value: 0 (fullmode, both channels (sawtooth and reverse sawtooth))
 */
#define CMD_DAC_TRANSFER_MODE       'P'

/** \brief Command returns pdc_read_tx_counter of DAC PDC, which tells you where in LUT 
 *         is PDC currently(PDC TCR). Can be used for setting LUT table on the fly. So 
 *         with slow enough period (sending LUT values is very slow) you can send LUT 
 *         values while they are being outputted.\n
 *         Command string: “U\\n\\r”\n
 *         Return: “#{TcrValue}”
 */
#define CMD_DAC_LUT_COUNTER         'U'


/* Parameter limits */
/** \brief Maximum number of parameters supported */
#define MAX_PARAMETER_COUNT         4
/** \brief Maximum parameter length "9999999" */
#define MAX_PARAMETER_LENGHT        7
/** \brief Maximum time to wait for all parameters */
#define PARAMETER_TIMEOUT           100


/* Parameter ranges */
/** \brief Parameter mode minimum value */
#define MODE_LOWRANGE               0
/** \brief Parameter mode maximum value */
#define MODE_HIGHRANGE              1
/** \brief Parameter sample period minimum value */
#define SAMPLE_PERIOD_LOWRANGE      1
/** \brief Parameter sample period maximum value */
#define SAMPLE_PERIOD_HIGHRANGE     1000000
/** \brief Parameter minimum number of averages */
#define AVERAGE_COUNT_LOWRANGE      1
/** \brief Parameter maximum number of averages */
#define AVERAGE_COUNT_HIGHRANGE     1000
/** \brief Parameter minimum number of samples */
#define MEASURMENT_COUNT_LOWRANGE   0
/** \brief Parameter maximum number of samples */
#define MEASURMENT_COUNT_HIGHRANGE  1000
/** \brief Parameter sequencer minimum value */
#define SEQUENCER_LOWRANGE          0
/** \brief Parameter sequencer maximum value */
#define SEQUENCER_HIGHRANGE         4
/** \brief Parameter minimum value */
#define DAC_VALUE_PAR1_LOWRANGE    -10000
/** \brief Parameter maximum value */
#define DAC_VALUE_PAR1_HIGHRANGE    10000
/** \brief Parameter ADC resolution 12-bit value */
#define ADC_RES_12_BITS             0
/** \brief Parameter ADC resolution 10-bit value */
#define ADC_RES_10_BITS             1
/** \brief Parameter minimum block size */
#define BLOCK_SIZE_MIN              1
/** \brief Parameter maximum block size */
#define BLOCK_SIZE_MAX              1024
/** \brief Parameter minimum DAC chanell */
#define DAC_CH_LOWRANGE             1
/** \brief Parameter maximum DAC chanell */
#define DAC_CH_HIGHRANGE            2
/** \brief Parameter DAC value minimum value */
#define DAC_VALUE_MIN               0
/** \brief 12th bit is channel selection bit. Can be 1. */
#define DAC_VALUE_MAX               8191
/** \brief Paramater LUT locaction minimum value */
#define DAC_LUT_LOCATION_LOWRANGE   0
/** \brief Paramater LUT locaction maximum value */
#define DAC_LUT_LOCATION_HIGHRANGE  1023
/** \brief Parameter min number of DAC LUT repeats */
#define DAC_REPEAT_NUM_MIN          0
/** \brief Parameter max number of DAC LUT repeats */
#define DAC_REPEAT_NUM_MAX          0xFFFF
/** \brief Parameter DAC minimum sample period */
#define DAC_PERIOD_MIN              1
/** \brief Parameter DAC maximum sample period */
#define DAC_PERIOD_MAX              0xFFFF

/* Communication mode defines */
/** \brief Parameter ASCII mode value */
#define ASCII_MODE                  0
/** \brief Parameter BIN mode value */
#define BIN_MODE                    1

/* Logic states */
/** \brief Logic state true value */
#define TRUE  1
/** \brief Logic state false value */
#define FALSE 0


/****************************************************************************************
* Type definitions
****************************************************************************************/
/** \brief Command structure */
typedef struct
{
  uint8_t cmd;                            /**< Command indentifier                     */
  bool (*funcPtr)(int32_t*, daq_settings_t*, COM_t *comInterface);
                                          /**< Function pointer to the command handler */
  int32_t par[MAX_PARAMETER_COUNT];       /**< Parameters values                       */
}CMD_t;


/****************************************************************************************
* Function prototypes
****************************************************************************************/
/* Parser function */
bool parseCommand (uint8_t CMD, CMD_t *parsedCMD, COM_t *comInterface);


#endif /* PARSER_H_ */
/*********************************** end of parser.h ***********************************/